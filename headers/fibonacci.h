#include <stdio.h>
int fib(int n){
    if(n<2){
        return n;
    }else{
        return fib(n-1) + fib(n-2);
    }
}

void fibonacci(int a){
    for (int i=0; i<a; i++){
        printf("%d ", fib(i));
    }
    puts("");
}
