#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "headers/factorial.h"
#include "headers/fibonacci.h"
#include "headers/ackermann.h"
#include "headers/proofByInduction.h"
int main(int argc, char **argv){
    double n, m;
    printf("Enter a number: ");
    scanf("%lf", &n);
//    printf("Enter another integer: ");
//    scanf("%d", &m);
    if (strcmp(argv[1], "fac") == 0) printf("Result: %i\n", factorial(n));
    else if (strcmp(argv[1], "fib") == 0) fibonacci(n);
    else if (strcmp(argv[1], "ack") == 0) ackermann(n, m);
    else if (strcmp(argv[1], "induct") == 0) Induction(n);
}

